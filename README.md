# terraform-module-demo-module

This is a terraform child module, which exists to demonstrate how to extract a
child module from a root module.


## Structure

This repo contains the following files:
```
README.md  # this readme
LICENSE  # MIT license
servers.tf  # terraform file for "servers"
variables.tf  # input variables
outputs.tf  # output values
```

Note that the file called `servers.tf` only creates local text files under
`/tmp`, because this allows the demo to run locally without actually needing
cloud resources, an existing k8s cluster, or any other friction-inducing
requirements; if you have `git`, `terraform`, and the ability to clone things
from gitlab, this demo should work.


## Example Usage

(This repo is intended to be used from terraform-module-demo-root.)

To use this module to create an environment of "servers", add something like the
following to your terraform file:
```
module "demo" {
  source  = "terraform-module-demo-module"
  version = "1.2.0"

  environment_name = "staging"
}
```
then proceed with the usual terraform init/plan/apply.


## Argument Reference

The following arguments are supported:

- `environment_name`- (Required) Name to use for 'environment' (directory) to create.
- `backend_server_count`- (Optional) Number of backend 'servers' (files) to create. Defaults to 2 if not set.
- `frontend_server_count`- (Optional) Number of frontend 'servers' (files) to create. Defaults to 2 if not set.

## Attributes Exported

The following attribute is exported:

- `directory` - Path to created directory.


## License

This repo is available under the MIT license; see LICENSE for details.

