output "directory" {
  description = "Created environment directory"
  value = "/tmp/terraform-module-demo-${var.environment_name}"
}

