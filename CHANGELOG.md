# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2022-07-27
### Added
- Added support for setting the number of servers via variables
  `backend_server_count` and `frontend_server_count` (both default to 2)

## [1.1.0] - 2022-07-25
### Added
- Added changelog.
### Changed
- Reformat readme to mirror official terraform docs.

## [1.0.1] - 2022-07-25
### Changed
- Mentioned `outputs.tf` in readme.
### Removed
- Removed `.terraform-version` file, which didn't do anything here.

## [1.0.0] - 2022-07-25
### Added
- Created module.
