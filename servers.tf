resource "local_file" "server_backend" {
  count    = var.backend_server_count
  content  = "I'm a stand-in for a backend server"
  filename = "/tmp/terraform-module-demo-${var.environment_name}/backend-${count.index + 1}"
}

moved {
  from = local_file.server_backend_1
  to   = local_file.server_backend[0]
}
moved {
  from = local_file.server_backend_2
  to   = local_file.server_backend[1]
}

resource "local_file" "server_frontend" {
  count    = var.backend_server_count
  content  = "I'm a stand-in for a frontend server"
  filename = "/tmp/terraform-module-demo-${var.environment_name}/frontend-${count.index + 1}"
}

moved {
  from = local_file.server_frontend_1
  to   = local_file.server_frontend[0]
}
moved {
  from = local_file.server_frontend_2
  to   = local_file.server_frontend[1]
}
