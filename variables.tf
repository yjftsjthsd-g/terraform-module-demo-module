variable "environment_name" {
  default = "staging"
}
variable "backend_server_count" {
  default = 2
}
variable "frontend_server_count" {
  default = 2
}
